package com.juwitatheomas_10191043.praktikum2.Adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.juwitatheomas_10191043.praktikum2.Fragment.LandingFragment;
import com.juwitatheomas_10191043.praktikum2.Fragment.ProgressFragment;

public class FragmentAdapter extends FragmentPagerAdapter {
    private Context context;
    private int tabsCount;

    public FragmentAdapter(Context context, int tabsCount, FragmentManager fragmentManager) {
        super(fragmentManager);

        this.context = context;
        this.tabsCount = tabsCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                LandingFragment landingFragment = new LandingFragment();
                return landingFragment;
            case 1:
                ProgressFragment progressFragment = new ProgressFragment();
                return progressFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabsCount;
    }
}
